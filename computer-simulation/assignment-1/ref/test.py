from numpy import array, random
import matplotlib.pyplot as plt

x = []
y = []
n = 50000
q = 100
a = random.uniform(0,4,n)
a = a**2

for i in range(1,(n//q)+1):
    l = i*q
    x.append(l)
    y.append((a[0:l].sum()/l) * 4)


print(y[-1])
plt.plot(x,y)
plt.xlabel('Random Numbers (n)')
plt.ylabel('Approximate Value (I)')
plt.show()